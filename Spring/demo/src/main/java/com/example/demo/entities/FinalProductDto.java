package com.example.demo.entities;

public class FinalProductDto {

	 
	private String productName;
	
	private String productType;
	
	private String productCategory;
	
	private long basePrice; 
	
	private int discount;
	private int gst;
	private int deliverycharges;
	private long finalPrice;
	
	
	public FinalProductDto() {
		super();
	}
	public FinalProductDto(String productName, String productType, String productCategory, long basePrice, int discount,
			int gst, int deliverycharges, long finalPrice) {
		super();
		this.productName = productName;
		this.productType = productType;
		this.productCategory = productCategory;
		this.basePrice = basePrice;
		this.discount = discount;
		this.gst = gst;
		this.deliverycharges = deliverycharges;
		this.finalPrice = finalPrice;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	public long getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(long basePrice) {
		this.basePrice = basePrice;
	}
	public int getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public int getGst() {
		return gst;
	}
	public void setGst(int gst) {
		this.gst = gst;
	}
	public int getDeliverycharges() {
		return deliverycharges;
	}
	public void setDeliverycharges(int deliverycharges) {
		this.deliverycharges = deliverycharges;
	}
	public long getFinalPrice() {
		return finalPrice;
	}
	public void setFinalPrice(long finalPrice) {
		this.finalPrice = finalPrice;
	}
	@Override
	public String toString() {
		return "FinalProductDto [productName=" + productName + ", productType=" + productType + ", productCategory="
				+ productCategory + ", basePrice=" + basePrice + ", discount=" + discount + ", gst=" + gst
				+ ", deliverycharges=" + deliverycharges + ", finalPrice=" + finalPrice + "]";
	}
	
	
}
