package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entities.PricePlan;

public interface PricePlanDao extends JpaRepository<PricePlan, Integer>{
    
	 public PricePlan findByCategory(String category);
}
