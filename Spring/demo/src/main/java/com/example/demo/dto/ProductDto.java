package com.example.demo.dto;

public class ProductDto {

	private String productType;

	private String productCategory;

	private long basePrice;



	public ProductDto(String productType, String productCategory, long basePrice) {
		super();
		this.productType = productType;
		this.productCategory = productCategory;
		this.basePrice = basePrice;
	}

	public ProductDto() {
		super();
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public long getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(long basePrice) {
		this.basePrice = basePrice;
	}

}
