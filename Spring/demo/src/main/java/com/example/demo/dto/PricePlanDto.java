package com.example.demo.dto;

public class PricePlanDto {

	 private int discount;
	 
	 private int GST;
	 
	 private int deliveryCharges;

	public PricePlanDto() {
		super();
	}

	public PricePlanDto(int discount, int gST, int deliveryCharges) {
		super();
		this.discount = discount;
		GST = gST;
		this.deliveryCharges = deliveryCharges;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public int getGST() {
		return GST;
	}

	public void setGST(int gST) {
		GST = gST;
	}

	public int getDeliveryCharges() {
		return deliveryCharges;
	}

	public void setDeliveryCharges(int deliveryCharges) {
		this.deliveryCharges = deliveryCharges;
	}
	 
	 
	 
}
