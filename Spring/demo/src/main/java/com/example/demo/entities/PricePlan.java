package com.example.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class PricePlan {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int id;
	@Column
	private String category;
	@Column
	private int discount;
	@Column
	private int gst;
	@Column
	private int deliveryCharges;
	
	

	public PricePlan(String category, int discount, int GST, int deliveryCharges) {
		super();
		this.category = category;
		this.discount = discount;
		this.gst = GST;
		this.deliveryCharges = deliveryCharges;
	}

	public PricePlan(int id, String category, int discount, int GST, int deliveryCharges) {
		super();
		this.id = id;
		this.category = category;
		this.discount = discount;
		this.gst = GST;
		this.deliveryCharges = deliveryCharges;
	}

	public PricePlan() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public int getGST() {
		return gst;
	}

	public void setGST(int GST) {
		this.gst = GST;
	}

	public int getDeliveryCharges() {
		return deliveryCharges;
	}

	public void setDeliveryCharges(int deliveryCharges) {
		this.deliveryCharges = deliveryCharges;
	}

	@Override
	public String toString() {
		return "PricePlan [id=" + id + ", category=" + category + ", discount=" + discount + ", GST=" + gst
				+ ", deliveryCharges=" + deliveryCharges + "]";
	}

}
