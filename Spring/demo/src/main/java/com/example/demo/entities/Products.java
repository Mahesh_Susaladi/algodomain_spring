package com.example.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="products")
public class Products {
   
	@Id
	@Column
	private int productId;
	@Column
	private String productName;
	@Column
	private String productType;
	@Column
	private String productCategory;
	@Column
	private long basePrice;

	
	
	public Products(String productName, String productType, String productCategory, long basePrice) {
		super();
		this.productName = productName;
		this.productType = productType;
		this.productCategory = productCategory;
		this.basePrice = basePrice;
	}

	public Products(int productId, String productName, String productType, String productCategory, long basePrice) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productType = productType;
		this.productCategory = productCategory;
		this.basePrice = basePrice;
	}

	public Products() {
		super();
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public long getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(long basePrice) {
		this.basePrice = basePrice;
	}

	@Override
	public String toString() {
		return "Products [productId=" + productId + ", productName=" + productName + ", productType=" + productType
				+ ", productCategory=" + productCategory + ", basePrice=" + basePrice + "]";
	}
	
	
	
	
}
