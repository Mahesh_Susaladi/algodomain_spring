package com.example.demo.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.PricePlanDao;
import com.example.demo.dto.PricePlanDto;
import com.example.demo.entities.PricePlan;

@Transactional
@Service
public class PricePlanService {

	 @Autowired
	 private PricePlanDao dao;
	 
	 public PricePlan findById(int id)
	 {
		return dao.findById(id).orElse(null);
		 
	 }
	 
	 public PricePlan findByCategory(String category)
	 {
		return dao.findByCategory(category);
		 
	 }
	 public PricePlan addPlan(PricePlan newPlan)
	 {
		 PricePlan savedPlan=dao.save(newPlan);
		return savedPlan;
		 
	 }
	 public PricePlan updatePlan(PricePlanDto newPlan,String category)
	 {
		
		PricePlan existingPlan=dao.findByCategory(category);
		int deliverCharges=newPlan.getDeliveryCharges();
		int GST=newPlan.getGST();
		int discount=newPlan.getDiscount();
		
		if(deliverCharges!=0)
		existingPlan.setDeliveryCharges(newPlan.getDeliveryCharges());
	    
		if(discount!=0)
		   existingPlan.setDiscount(newPlan.getDiscount());
		if(GST!=0)
			existingPlan.setGST(newPlan.getGST());
		
		return dao.save(existingPlan);
		 
	 }
	 
	

}
