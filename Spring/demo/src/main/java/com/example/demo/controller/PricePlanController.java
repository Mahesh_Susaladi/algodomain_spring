package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.PricePlanDto;
import com.example.demo.entities.PricePlan;
import com.example.demo.service.PricePlanService;
import com.example.demo.utilities.Response;

@CrossOrigin
@RestController
public class PricePlanController {

	@Autowired
	private PricePlanService service;
	
	@PostMapping("/pricePlan/add")
	ResponseEntity<?>addProduct(@RequestBody PricePlan newPlan)
	{
		PricePlan plan=service.addPlan(newPlan);
		 if(plan!=null)
			 return Response.success(plan);
		 
		return Response.error("Operation Failed!");
		
	}
	
	@GetMapping("/pricePlan/get/{category}")
	ResponseEntity<?> getPlan(@PathVariable String category)
	{
		PricePlan plan=service.findByCategory(category);
		
		if(plan==null)
			return Response.error("No Products Found");
		
		return Response.success(plan);
					
	}
		
	@PutMapping("/pricePlan/update/{category}")
	ResponseEntity<?> updateProduct(@RequestBody PricePlanDto newPlan,@PathVariable  String category)
	{
		PricePlan plan=service.updatePlan(newPlan,category);
		
		if(plan!=null)
			 return Response.success(plan);
		 
		return Response.error("Update Failed!");
				
	}
}
