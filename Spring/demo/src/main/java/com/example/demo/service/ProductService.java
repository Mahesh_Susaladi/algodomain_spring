package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ProductDao;
import com.example.demo.dto.ProductDto;
import com.example.demo.entities.FinalProductDto;
import com.example.demo.entities.PricePlan;
import com.example.demo.entities.Products;

@Transactional
@Service
public class ProductService {

	@Autowired
	private ProductDao dao;

	@Autowired
	PricePlanService planService;

	public Products addProduct(Products newProduct) {
		Products savedProduct = dao.save(newProduct);
		return savedProduct;
	}

	public Products findProduct(int id) {
		Products savedProduct = dao.findByProductId(id);
		return savedProduct;
	}

	public boolean deleteProduct(int id) {

		dao.deleteById(id);
		return true;
	}

	public Products updateProduct(int id, ProductDto newProduct) {

		String productType = newProduct.getProductType();

		String productCategory = newProduct.getProductCategory();

		long basePrice = newProduct.getBasePrice();

		Products existingProduct = dao.findByProductId(id);

		if (productType != null)
			existingProduct.setProductType(productType);
		if (productCategory != null)
			existingProduct.setProductCategory(productCategory);
		if (basePrice != 0)
			existingProduct.setBasePrice(basePrice);

		return dao.save(existingProduct);

	}

	public List<Products> findAll() {
		return dao.findAll();
	}

	public FinalProductDto calculateFinalPrice(Products product) {

		String category = product.getProductCategory();
		
		// get plan for category
		PricePlan plan = planService.findByCategory(category);
        
		
		// calculate discount
		int discount = (int) ((product.getBasePrice()) * (plan.getDiscount()) / 100);
		

		// reduce discount from base price
		long basePrice = product.getBasePrice() - discount;

		// calculate gst on discounted price
		int gst = (int) ((basePrice) * (plan.getGST()) / 100);
	
		// get delivery charges
		int deliveryCharges = plan.getDeliveryCharges();

		// add all to final price
		long finalPrice = basePrice + gst + deliveryCharges;
	
		return new FinalProductDto(product.getProductName(), product.getProductType(), product.getProductCategory(),
				product.getBasePrice(), discount, gst, deliveryCharges, finalPrice);

	}

}
